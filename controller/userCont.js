const { User, validate } = require("../models/user");
const bcrypt = require("bcryptjs");

async function get(req, res) {
  user = await User.find().select("-password");
  res.status(200).json(user);
}

async function add(req, res) {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let user = await User.findOne({ email: req.body.email });
  if (user)
    return res.status(400).json({ message: "User is already registered" });

  user = new User({
    email: req.body.email,
    password: req.body.password
  });
  const salt = await bcrypt.genSalt(10);
  user.password = await bcrypt.hash(user.password, salt);

  await user.save();
  res.status(201).json(user);
}

async function login(req, res) {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let user = await User.findOne({ email: req.body.email });
  if (!user) return res.status(400).send("Invalid email or password.");

  const validPassword = await bcrypt.compare(req.body.password, user.password);
  if (!validPassword) return res.status(400).send("Invalid email or password");

  const validUser = true;
  res.status(200).json({ message: validUser });
}

module.exports = { get, add, login };
