const { Task, validateTask } = require("../models/task");

async function get(req, res) {
  const task = await Task.find();
  res.status(200).json(task);
}

async function getId(req, res) {
  const task = await Task.findById(req.params.id);
  res.status(200).json(task);
}

async function add(req, res) {
  const { error } = validateTask(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const task = new Task({
    task: req.body.task,
    description: req.body.description,
    status: req.body.status
  });
  await task.save();
  res.status(201).json(task);
}

async function update(req, res) {
  const { error } = validateTask(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const task = await Task.findByIdAndUpdate(
    req.params.id,
    {
      task: req.body.task,
      description: req.body.description,
      status: req.body.status
    },
    { new: true }
  );
  if (!task) return res.status(404).send("Task not found");
  res.status(200).json(task);
}

async function del(req, res) {
  let task = await Task.findByIdAndDelete(req.params.id);
  if (!task) return res.status(404).send("Task not found");

  res.status(200).send("Task Deleted");
}

module.exports = { get, getId, add, update, del };
