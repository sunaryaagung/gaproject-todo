process.env.NODE_ENV = "test";

let bcrypt = require("bcryptjs");
let mongoose = require("mongoose");
let { Task } = require("../models/task");

//dev defendencies
let chai = require("chai");
let chaiHttp = require("chai-http");
let server = require("../index");
let should = chai.should();
let faker = require("faker");
const salt = bcrypt.genSaltSync(10);

chai.use(chaiHttp);

describe("Task", () => {
  beforeEach(done => {
    Task.deleteOne({}, err => {
      done();
    });
  });
  describe("/Get Task", () => {
    it("It should return list of task", done => {
      chai
        .request(server)
        .get("/api/tasks")
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
  });
  describe("/Post Task", () => {
    it("It should create a new task", done => {
      let task = { task: "test task" };
      chai
        .request(server)
        .post("/api/tasks")
        .send(task)
        .end((err, res) => {
          res.should.have.status(201);
          res.body.should.be.a("object");
          res.body.should.have.property("task");
          res.body.should.have.property("status");
          done();
        });
    });
  });
  describe("/Get/:id Task", () => {
    it("It should update a task", done => {
      let task = new Task({ task: "first task" });
      task.save((err, task) => {
        chai
          .request(server)
          .get("/api/tasks/" + task._id)
          .send(task)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.have.property("task");
            res.body.should.have.property("status");
            done();
          });
      });
    });
  });
  describe("/Put/:id Task", () => {
    it("it should update task", done => {
      let task = new Task({ task: "first task" });
      task.save((err, task) => {
        chai
          .request(server)
          .put("/api/tasks/" + task._id)
          .send({ task: "edited" })
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.have.property("task");
            res.body.should.have.property("status");
            done();
          });
      });
    });
  });
  describe("/Delete/:id Task", () => {
    it("it should delete task", done => {
      let task = new Task({ task: "first task" });
      task.save((err, task) => {
        chai
          .request(server)
          .delete("/api/tasks/" + task._id)
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });
});
