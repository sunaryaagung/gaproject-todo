const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
  task: { type: String, minlength: 1, required: true },
  description: { type: String, minlength: 1 },
  status: { type: Boolean, default: false }
});

const Task = new mongoose.model("Task", taskSchema);

//VALIDATOR
const Joi = require("@hapi/joi");

function validateTask(task) {
  const schema = {
    task: Joi.string()
      .min(1)
      .required(),
    description: Joi.string().min(1),
    status: Joi.boolean()
  };
  return Joi.validate(task, schema);
}

exports.Task = Task;
exports.validateTask = validateTask;
