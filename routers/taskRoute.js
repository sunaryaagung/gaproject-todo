const express = require("express");
const router = express.Router();
const { get, getId, add, update, del } = require("../controller/taskCont");
const validateObjectId = require("../middleware/validateObejctid");

router.get("/", get);
router.get("/:id", validateObjectId, getId);
router.post("/", add);
router.put("/:id", validateObjectId, update);
router.delete("/:id", validateObjectId, del);

module.exports = router;
