const express = require("express");
const router = express.Router();
const { get, add, login } = require("../controller/userCont");

router.get("/", get);
router.post("/", add);
router.post("/login", login);

module.exports = router;
