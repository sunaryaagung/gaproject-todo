const express = require("express");
const router = express.Router();
const task = require("./taskRoute");
const user = require("./userRoute");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("../swagger.json");

router.use("/tasks", task);
router.use("/users", user);
router.use("/documentation", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

module.exports = router;
